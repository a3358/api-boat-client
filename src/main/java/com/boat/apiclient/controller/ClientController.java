package com.boat.apiclient.controller;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.boat.apiclient.model.dto.ClientDTO;
import com.boat.apiclient.model.dto.ClientUpdateDTO;
import com.boat.apiclient.service.ClientService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
public class ClientController {

	private ClientService service;


	public ClientController(ClientService service) {
		this.service = service;

	}

	@ApiOperation(value = "Busca um cliente pelo id")
	@GetMapping("/client/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<ClientDTO> getClientById(@PathVariable Long id) {
		ClientDTO clientById = service.getClientById(id);
		return ResponseEntity.ok().body(clientById);
	}

	@ApiOperation(value = "Salva um  cliente")
	@PostMapping("/client")
	public ResponseEntity<ClientDTO> save(@RequestBody @Valid ClientDTO dto) {
		ClientDTO client = service.save(dto);
		return ResponseEntity.status(HttpStatus.CREATED).body(client);

	}

	@ApiOperation(value = "Atualiza um cliente")
	@PutMapping("/client/{id}")
	public ResponseEntity<ClientDTO> update(@RequestBody ClientUpdateDTO dto, @PathVariable Long id) {
		ClientDTO updateClient = service.updateClient(id, dto);
		return ResponseEntity.ok().body(updateClient);
	}

	@ApiOperation(value = "Retorna uma lista de clientes")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retorna a lista de pessoa"),
			@ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"), })
	@GetMapping("cleints")
	public ResponseEntity<Page<ClientDTO>> getClients(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction
) {
//		Page<Client> list = service.getClients(page, linesPerPage);
//		Page<ClientDTO> listDto = list.map(item -> mapper.map(item, ClientDTO.class));
		Page<ClientDTO> clients = service.getClients(page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(clients);
	}

	@ApiOperation(value = "Deleta um cliente")
	@DeleteMapping("/client/{id}")
	public ResponseEntity<ClientDTO> deleteCliente(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
