package com.boat.apiclient.controller.exceptionHandle;

import java.io.Serializable;

public class ExceptionError implements Serializable {
	private static final long serialVersionUID = 1L;

	private String error;

	public ExceptionError(String errorString) {
		this.error = errorString;
	}

	public String getErrorString() {
		return error;
	}

}
