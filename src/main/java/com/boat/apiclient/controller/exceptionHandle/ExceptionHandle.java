package com.boat.apiclient.controller.exceptionHandle;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.boat.apiclient.service.exception.OjectNotFoundException;

@RestControllerAdvice
public class ExceptionHandle extends ResponseEntityExceptionHandler {
	@ExceptionHandler({ OjectNotFoundException.class })
	public ResponseEntity<?> errorNotFound(Exception e) {
		return ResponseEntity.notFound().build();

	}

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return new ResponseEntity<>(new ExceptionError("Operação não permitida"), HttpStatus.METHOD_NOT_ALLOWED);

	}

}
