package com.boat.apiclient.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.boat.apiclient.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	public Optional<Client> findClientByCpf(String cpf);
	public Optional<Client> findClientByEmail(String email);
}
