package com.boat.apiclient.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.boat.apiclient.model.enums.SexoEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
///@Table(name="CAD_CLIENT")
public class Client {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String nameMom;
//	@JsonFormat(pattern = "dd/MM/yyyy")
//	@Temporal(TemporalType.DATE)
//	private Date dateBirth;
	@Enumerated(EnumType.STRING)
	private SexoEnum sex;
	private String cpf;
	private String email;
}
