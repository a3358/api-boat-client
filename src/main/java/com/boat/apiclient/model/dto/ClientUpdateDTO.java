package com.boat.apiclient.model.dto;

import com.boat.apiclient.model.enums.SexoEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ClientUpdateDTO {
	private String name;
	private String nameMom;
	private String email;
	private SexoEnum sex;

}
