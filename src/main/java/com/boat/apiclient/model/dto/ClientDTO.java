package com.boat.apiclient.model.dto;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import com.boat.apiclient.model.enums.SexoEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@NotBlank(message = "Preenchimento obrigatório")
	@Length(min = 5, max = 120, message = "O tamanho deve ser 5  e 120 caracteres")
	private String name;
	@NotBlank(message = "Preenchimento obrigatório")
	@Length(min = 5, max = 120, message = "O tamanho deve ser 5  e 120 caracteres")
	private String nameMom;
	//@NotEmpty(message = "Preenchimento obrigatório")
	@Enumerated(EnumType.STRING)
	private SexoEnum sex;
	@CPF
	private String cpf;
	@Email
	private String email;	
}
