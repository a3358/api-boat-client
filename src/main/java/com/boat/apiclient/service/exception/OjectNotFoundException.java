package com.boat.apiclient.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class OjectNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public OjectNotFoundException(String msg) {
		super(msg);
	}

	public OjectNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}


}
