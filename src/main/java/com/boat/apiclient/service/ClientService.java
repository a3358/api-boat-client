package com.boat.apiclient.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.boat.apiclient.model.Client;
import com.boat.apiclient.model.dto.ClientDTO;
import com.boat.apiclient.model.dto.ClientUpdateDTO;
import com.boat.apiclient.repository.ClientRepository;
import com.boat.apiclient.service.exception.BusinessException;

@Service
public class ClientService {

	private ClientRepository repository;

	private ModelMapper mapper;

	@Autowired
	public ClientService(ClientRepository repository, ModelMapper mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	public ClientDTO getClientById(Long id) {
		Client client = repository.findById(id).orElseThrow(() -> new BusinessException("Cliente não encontrado"));
		return mapper.map(client, ClientDTO.class);

	}

	public ClientDTO save(ClientDTO dto) {
		Client client = mapper.map(dto, Client.class);
		if (repository.findClientByCpf(dto.getCpf()).isPresent()) {
			throw new BusinessException("CPF já cadastrado");
		} else {
			if (repository.findClientByEmail(dto.getEmail()).isPresent()) {
				throw new BusinessException("EMAIL já cadastrado");
			} else {

				return mapper.map(repository.save(client), ClientDTO.class);

			}

		}

	}

	public Page<ClientDTO> getClients(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Page<Client> list = repository.findAll(pageRequest);
		return list.map(item -> mapper.map(item, ClientDTO.class));
	}

	public ClientDTO updateClient(Long id, ClientUpdateDTO dto) {
		Client client = repository.findById(id).orElseThrow(() -> new BusinessException("Cliente não encontrado"));
		mapper.map(dto, client);
		return mapper.map(repository.save(client), ClientDTO.class);

	}

	public void delete(Long id) {
		this.repository.deleteById(id);
	}
}
