package com.boat.apiclient.feature;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import com.boat.apiclient.model.Client;
import com.boat.apiclient.model.dto.ClientDTO;
import com.boat.apiclient.model.dto.ClientUpdateDTO;
import com.boat.apiclient.model.enums.SexoEnum;

public class ScenarioFactory {

	public static ClientDTO newClientClientDTO() {
		return ClientDTO.builder().name("Marcos").nameMom("Izabel").sex(SexoEnum.MASCULINO).cpf("20033135088")
				.email("marcos@gmail.com").build();
	}

	public static Client newClient() {
		return Client.builder().name("Marcos").nameMom("Izabel").sex(SexoEnum.MASCULINO).cpf("20033135088")
				.email("marcos@gmail.com").build();
	}

	public static Client newSavedClient() {
		var user = newClient();
		user.setId(1l);
		return user;
	}

	public static ClientDTO newSavedClientDTO() {
		var client = newClientClientDTO();
		client.setId(1l);
		return client;
	}

	public static Optional<Client> newOptionalClien() {
		return Optional.of(newSavedClient());
	}

	public static ClientDTO newClientId() {
		var client = newClientClientDTO();
		client.setId(1l);
		return client;
	}

	public static Long idClient() {
		Long id = 1L;
		return id;
	}

	public static ClientDTO clienteNewDTO() {
		var client = newClientClientDTO();
		client.setId(1l);
		return client;

	}

	public static ClientDTO ClientDTO() {
		var client = newClientClientDTO();
		client.setId(1l);
		return client;

	}

	public static Long clientDelet() {
		Long id = null;
		return id;
	}

	public static ClientDTO clientDto() {
		var client = new ClientDTO();
		return client;
	}

	public static Client updatedClient() {
		return Client.builder().name("Marcos").nameMom("Izabel").sex(SexoEnum.MASCULINO).cpf("20033135088")
				.email("marcos@gmail.com").build();
	}

	public static ClientUpdateDTO newUpdateClientDTO() {
		return ClientUpdateDTO.builder().name("Teste012").nameMom("teste0123").email("marcos@gmail.com")
				.sex(SexoEnum.FEMININO).build();

	}

	public static Client client() {
		var client = new Client();
		return client;
	}

	public static Page<Client> newPage() {
		List<Client> client = new ArrayList<>();
		client.add(client());
		Page<Client> page = new PageImpl<>(client);
		return page;
	}

	public static PageRequest pageRequest() {
		PageRequest list = PageRequest.of(0, 5, Direction.valueOf("ASC"), "sssssss");
		return list;
	}
}
