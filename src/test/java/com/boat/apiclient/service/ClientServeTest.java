package com.boat.apiclient.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertNotNull;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.boat.apiclient.feature.ScenarioFactory;
import com.boat.apiclient.model.Client;
import com.boat.apiclient.model.dto.ClientDTO;
import com.boat.apiclient.model.dto.ClientUpdateDTO;
import com.boat.apiclient.model.enums.SexoEnum;
import com.boat.apiclient.repository.ClientRepository;
import com.boat.apiclient.service.exception.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
public class ClientServeTest {

	@InjectMocks
	ClientService service;
	@Mock
	ClientRepository repository;

	@Mock
	private ModelMapper mapper;

	@Test
	public void getClientById_whenSendIdClientValid_ExpectedClient() {
		Client newClient = ScenarioFactory.newClient();
		ClientDTO clienteNewDTO = ScenarioFactory.clienteNewDTO();
		ClientDTO clientDTO = ScenarioFactory.ClientDTO();
		Long id = ScenarioFactory.idClient();

		when(repository.findById(id)).thenReturn(Optional.of(newClient));
		when(mapper.map(newClient, ClientDTO.class)).thenReturn(clientDTO);

		ClientDTO clientById = service.getClientById(id);
		when(mapper.map(clientById, ClientDTO.class)).thenReturn(clienteNewDTO);
		assertEquals(clientDTO, clienteNewDTO);
		assertThat(clienteNewDTO.getId()).isNotNull();
		assertThat(clienteNewDTO.getId()).isEqualTo(1L);
		assertThat(clienteNewDTO.getName()).isEqualTo("Marcos");
		assertThat(clienteNewDTO.getNameMom()).isEqualTo("Izabel");
		assertThat(clienteNewDTO.getSex()).isEqualTo(SexoEnum.MASCULINO);
		assertThat(clienteNewDTO.getCpf()).isEqualTo("20033135088");
		assertThat(clienteNewDTO.getEmail()).isEqualTo("marcos@gmail.com");

	}

	@Test
	public void getClientById_whenClientIdNotExist_ExpectedExeception() {
		Long id = 1L;

		when(repository.findById(id)).thenThrow(new BusinessException("Cliente não encontrado"));

		assertThatThrownBy(() -> service.getClientById(id)).isInstanceOf(BusinessException.class)
				.hasMessage("Cliente não encontrado");

		verify(repository, times(1)).findById(id);

	}

	@Test
	public void save_whenReceiveValidClient_ExpectedSuccess() {
		// cenario
		ClientDTO clientDTO = ScenarioFactory.newClientClientDTO();
		Client client = ScenarioFactory.newClient();
		Client savedClient = ScenarioFactory.newSavedClient();
		ClientDTO savedClientDTO = ScenarioFactory.newSavedClientDTO();
		when(mapper.map(clientDTO, Client.class)).thenReturn(client);
		when(repository.save(client)).thenReturn(savedClient);
		when(mapper.map(savedClient, ClientDTO.class)).thenReturn(savedClientDTO);
		ClientDTO savedClientDTOResponse = service.save(clientDTO);

		assertNotNull(savedClientDTOResponse);
		verify(repository, times(1)).save(client);

	}

	@Test
	public void save_WhenReceiveClientWithAlreadyExistsCpf_ExpectedeException() {
		ClientDTO userDTO = ScenarioFactory.newClientClientDTO();
		Client client = ScenarioFactory.newClient();
		Optional<Client> findClientByCpf = ScenarioFactory.newOptionalClien();

		when(repository.findClientByCpf(userDTO.getCpf())).thenReturn(findClientByCpf);
		when(mapper.map(userDTO, Client.class)).thenReturn(client);

		assertThatThrownBy(() -> service.save(userDTO)).isInstanceOf(BusinessException.class)
				.hasMessage("CPF já cadastrado");
		verify(repository, times(1)).findClientByCpf(userDTO.getCpf());

	}

	@Test
	public void save_WhenReceiveClientWithAlreadyExistsEmail_ExpectedeException() {
		ClientDTO userDTO = ScenarioFactory.newClientClientDTO();
		Client client = ScenarioFactory.newClient();
		Optional<com.boat.apiclient.model.Client> findClientByEmail = ScenarioFactory.newOptionalClien();

		when(repository.findClientByEmail(userDTO.getEmail())).thenReturn(findClientByEmail);
		when(mapper.map(userDTO, Client.class)).thenReturn(client);

		assertThatThrownBy(() -> service.save(userDTO)).isInstanceOf(BusinessException.class)
				.hasMessage("EMAIL já cadastrado");
		verify(repository, times(1)).findClientByEmail(userDTO.getEmail());

	}

	@Test
	public void updateClient_WhenSendValidId_ExcepctedeSucess() {
		// cenario
		var updateClientDTO = ScenarioFactory.newUpdateClientDTO();
		var optionalClien = ScenarioFactory.newOptionalClien();
		// executar
		when(repository.findById(optionalClien.get().getId())).thenReturn(optionalClien);
		when(repository.save(optionalClien.get())).thenReturn(optionalClien.get());
		when(repository.save(optionalClien.get())).thenReturn(optionalClien.get());
		when(mapper.map(optionalClien.get(), ClientDTO.class)).thenReturn(ScenarioFactory.newClientClientDTO());
		// validar
		ClientDTO update = service.updateClient(optionalClien.get().getId(), updateClientDTO);
		assertNotNull(update);
		verify(repository, times(1)).findById(optionalClien.get().getId());
		verify(mapper, times(1)).map(updateClientDTO, optionalClien.get());
		verify(repository, times(1)).save(optionalClien.get());
		verify(mapper, times(1)).map(optionalClien.get(), ClientDTO.class);

	}

	@Test
	public void updateClient_whenUpdateClientWithInvalid_ExcepctedeException() {
		// cenario
		ClientUpdateDTO updateClient = ScenarioFactory.newUpdateClientDTO();
		Client client = ScenarioFactory.newSavedClient();

		// executar
		when(repository.findById(client.getId())).thenThrow(new BusinessException("Cliente não encontrado"));

		// validação

		assertThatThrownBy(() -> service.updateClient(client.getId(), updateClient))
				.isInstanceOf(BusinessException.class).hasMessage("Cliente não encontrado");

		verify(repository, times(1)).findById(client.getId());
	}

	@Test
	public void delete_whenDeleteWithSucess_ExcepctedeSucess() {

		assertDoesNotThrow(() -> service.delete(ScenarioFactory.newSavedClient().getId()));

		verify(repository, times(1)).deleteById(ScenarioFactory.newSavedClient().getId());
	}

	@Test
	public void delete_whenDeleteWithInvalid_ExcepctedeException() {
		Long idClient = ScenarioFactory.clientDelet();
		Mockito.verify(repository, Mockito.never()).deleteById(idClient);

	}

	@Test
	public void page_whenPage_Excepctede() {
		var newPage = ScenarioFactory.newPage();
		var pageRequest = ScenarioFactory.pageRequest();
		var page = 0;
		var linesPerPage = 5;
		var orderBy = "sssssss";
		var direction = "ASC";
		
		when(repository.findAll(pageRequest)).thenReturn(newPage);
		when(mapper.map(newPage, ClientDTO.class)).thenReturn(ScenarioFactory.clientDto());
		
		service.getClients(page, linesPerPage, orderBy, direction);
		
		verify(repository, times(1)).findAll(pageRequest);
		

	}

}
